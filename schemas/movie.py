from typing import Optional, List
from pydantic import BaseModel, Field

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(
        min_length=1,
        max_length=20
        )
    overview: str = Field(
        min_length=50,
        max_length=255
    )
    year: int = Field(
        ge=1950,
        le=2030
    )
    rating: float = Field(
        gt=0,
        lt=10,
    )
    category: str = Field(
        min_length=5,
        max_length=50
    )
    class config:
        schema_default = {
            "example" : {
                "id":1,
                "title":"Título pelicula",
                "overview":"Descripción de la película",
                "year":2022,
                "rating":9.8,
                "category":"Acción"
                }
            }